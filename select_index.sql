SELECT
    us.id,
    us.username,
    sum(count_likes) as cnt_all_likes
FROM users us
INNER JOIN video vid ON vid.user_id = us.id
group by us.id, us.username;

SELECT
       *
FROM comments;

INSERT INTO users (id, email, username, password) VALUES (1, 'email@', 'yes', 'no');
INSERT INTO users (id, email, username, password) VALUES (2, '2email@', 'rest', 'ter');

UPDATE users SET email = '123@mail' WHERE id = 1;
UPDATE users SET username = 'iam' WHERE id = 1;
UPDATE users SET password = 12421 WHERE id = 1;

INSERT INTO chat (id, user_id_1, user_id_2, message, date) VALUES (1, 1, 2, 'u dump', '1970-01-01 00:00:00'::date);
INSERT INTO chat (id, user_id_1, user_id_2, message, date) VALUES (2, 2, 1, 'no u', '1970-01-01 00:00:00'::date);

UPDATE video SET count_likes = count_likes + 1 WHERE id = 1;

CREATE INDEX index_video_user_id ON video (user_id);
-- Чтобы быстро искать все видео пользователя, банально мы открываем профиль пользователя и на главной сразу же видим все его записи, и так постоянно, нам важно уметь быстро их доставать
