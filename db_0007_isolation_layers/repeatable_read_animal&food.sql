BEGIN;

DELETE FROM Animal_Food WHERE id_animal = 1 AND id_food = 1;
INSERT INTO Animal_Food (id_animal, id_food) VALUES (1, 2);

COMMIT;

-- Мы изменяем строку в табдице, меняем посредством удаления и добавления новой (да можно по-другому, но у нас так
-- Соответственно, нам важно, чтобы никто не прочитал нашу транзакцию на этапе "после удаления", нужно, чтобы они
-- читали, уже после завершения транзакции, а именно после замены строки, А ИМЕННО после INSTRT-а
-- Транзакция 2 - это обычное чтение этой таблицы