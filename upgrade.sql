BEGIN TRANSACTION;

CREATE TABLE subscribe (
    user_id INT NOT NULL,
    sub_user_id INT NOT NULL,
    FOREIGN KEY (user_id) references users (id) ON DELETE RESTRICT,
    FOREIGN KEY (sub_user_id) references users (id) ON DELETE RESTRICT
);

COMMIT;
