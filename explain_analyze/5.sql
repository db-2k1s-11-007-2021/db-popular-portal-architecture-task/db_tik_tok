CREATE INDEX ON tickets(ticket_no);
CREATE INDEX ON ticket_flights(ticket_no);
CREATE INDEX ON airports_data(airport_code);
CREATE INDEX ON flights(arrival_airport, departure_airport);
-- Создаем индексы для быстрой работы left join
SET work_mem TO '256MB';
-- Выделяем память, чтобы не использовалась дополнительная память на диске, которая замедляет работу запроса
SET enable_nestloop=off;
SET enable_hashjoin=on;
-- Выключаем nestloop, потому что он идет как вложенный цикл, и включаем функцию хеш,
-- его использование выходит эффективнее

EXPLAIN (ANALYZE, COSTS, VERBOSE, BUFFERS, FORMAT JSON)
with
     first_moscow_flights as (
        select
            passenger_id,
            min(scheduled_departure) as moscow_first_flight_dt
        from tickets t
        left join ticket_flights tf on t.ticket_no = tf.ticket_no
        left join flights f on f.flight_id = tf.flight_id
        left join airports a1 on f.arrival_airport = a1.airport_code
        left join airports a2 on f.departure_airport = a2.airport_code
        where a1.city = 'Moscow' or a2.city = 'Moscow'
        group by passenger_id
        having min(scheduled_departure) is not null
     ),

    last_non_moscow_flights as (
        select
            passenger_id,
            max(scheduled_departure) as non_moscow_last_flight_dt
        from tickets t
        left join ticket_flights tf on t.ticket_no = tf.ticket_no
        left join flights f on f.flight_id = tf.flight_id
        left join airports a1 on f.arrival_airport = a1.airport_code
        left join airports a2 on f.departure_airport = a2.airport_code
        where a1.city <> 'Moscow' and a2.city <> 'Moscow'
        group by passenger_id
        having max(scheduled_departure) is not null
    ),

    non_moscow_after_moscow_flights as (
        select
            count(distinct m.passenger_id) cnt
        from first_moscow_flights m
        left join last_non_moscow_flights n on m.passenger_id = n.passenger_id
        where moscow_first_flight_dt < non_moscow_last_flight_dt
    )

select
    cnt * 100.0 / count(distinct passenger_id) as percent
from tickets
left join non_moscow_after_moscow_flights on 1=1
group by cnt;
